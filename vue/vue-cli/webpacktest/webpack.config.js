const path = require('path');

module.exports = {
    entry:"./hello.js",
    output:{
        path:path.resolve(__dirname,"dist"),
        filename:'my-first-webpack.bundle.js'//打包最终输出的文件名

    },
    module: {
        rules: [
          { test: /\.css$/, use: 'style-loader!css-loader'},
          { test: /\.ts$/, use: 'ts-loader' }
        ]
      }
};