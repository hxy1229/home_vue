    const app = require（'http'）.createServer（handler）
    //require前面括号：获取socketio对象,之后调用app  
    //const io = require（'socket.io'）（app）; 
    var socketio = require('socket.io');
    //实例化一个io，通过io实现数据双向的绑定连接
    var io = socketio(app);
    const fs = require（'fs'）;

    app.listen（80）;

//  处理web服务器正常的请求
    function handler(req，res) {
    fs.readFile（__ dirname + '/ index.html '，
    （err，data）=> {
        if（err）{ 
            res.writeHead（ 500）;
            //返回
            res.end（ '错误加载索引.html'）;
    
    }

        res.writeHead（200）;
        res.end（data）;
    } ）;
    }
        //实时通讯连接
        //io.on('connection',事件的回调函数),监听socketio的连接事件
    io.on（'connection'，（socket）=> {
        //发送emit给客户端一个news事件，将事件对象发送给客户端（发送客户端数据）===>hello world
    socket.emit（'news'，{ hello：'world' } ）;
    //监听客户端发送过来的内容
    socket.on（'my other event'，（data）=> { console.log （数据）; } ）;
    } ）;